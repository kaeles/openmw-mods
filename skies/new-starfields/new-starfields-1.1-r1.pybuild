# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os

from common.mw import MW, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "New Starfields"
    DESC = "A simple, high-definition replacer for the clear night sky texture"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/43246"
    LICENSE = "all-rights-reserved"
    RDEPEND = "skies/swg-skies[night-sky-mesh]"
    KEYWORDS = "openmw"
    NEXUS_SRC_URI = """
        opacity-100? (
            https://www.nexusmods.com/morrowind/mods/43246?tab=files&file_id=1000002807
            -> New_Starfields-43246-1-1.rar
        )
        !opacity-100? (
            https://www.nexusmods.com/morrowind/mods/43246?tab=files&file_id=1000002920
            ->  New_Starfields_-_Alternate_Opacities-43246-1-0.7z
        )
    """
    RESTRICT = "!opacity-100? ( fetch )"
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/43246"
    TEXTURE_SIZES = """
        !variant-5? ( 4096 )
        variant-5? ( 8192 )
    """
    IUSE = """
        +variant-0
        variant-1
        variant-2
        variant-3
        variant-4
        variant-5
        variant-6
        variant-7
        opacity-25
        opacity-33
        opacity-50
        +opacity-100
    """
    REQUIRED_USE = """
        ^^ (
            variant-0
            variant-1
            variant-2
            variant-3
            variant-4
            variant-5
            variant-6
            variant-7
        )
        ^^ (
            opacity-25
            opacity-33
            opacity-50
            opacity-100
        )
    """
    S = "New_Starfields-43246-1-1"
    INSTALL_DIRS = [
        InstallDir(
            ".",
            S="New_Starfields-43246-1-1",
            RENAME="textures",
            WHITELIST=["tx_stars.dds"],
        )
    ]

    def src_prepare(self):
        for flag in self.USE:
            # Note that exactly one flag will match due to REQUIRED_USE
            if flag.startswith("opacity-"):
                opacity = flag.lstrip("opacity-")

        for flag in self.USE:
            # Note that exactly one flag will match due to REQUIRED_USE
            if flag.startswith("variant-"):
                variant = "0" + flag.lstrip("variant-")

        dest = os.path.join(self.WORKDIR, self.S, "tx_stars.dds")
        os.makedirs(os.path.dirname(dest), exist_ok=True)
        if "opacity-100" in self.USE:
            source = self.S
            os.rename(
                os.path.join(self.WORKDIR, source, f"tx_stars_{variant}.dds"), dest
            )
        else:
            source = "New_Starfields_-_Alternate_Opacities-43246-1-0"
            os.rename(
                os.path.join(self.WORKDIR, source, f"tx_stars_{variant}_{opacity}.dds"),
                dest,
            )
