# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import sys

from pybuild.info import PV, P

from common.mw import MW, File, InstallDir


class Package(MW):
    NAME = "OpenMW Leveled List Fixer"
    DESC = "Utility that automatically merges levelled lists from other mods"
    HOMEPAGE = "https://github.com/jmelesky/omwllf"
    LICENSE = "ISC"
    KEYWORDS = "openmw"
    SRC_URI = f"""
        https://github.com/jmelesky/omwllf/archive/v{PV}.tar.gz
        -> omwllf-{PV}.tar.gz
    """
    INSTALL_DIRS = [InstallDir(".", PLUGINS=[File("OMWLLF.omwaddon")])]
    TIER = "z"
    REBUILD_FILES = ["*.esp", "*.esm", "*.omwaddon", "*.omwgame"]
    PATCHES = "bytearray.patch"
    S = f"{P}/{P}"

    def src_prepare(self):
        super().src_prepare()
        print("Generating OMWLLF.omwaddon...")
        self.execute(f"{sys.executable} omwllf.py -m OMWLLF.omwaddon -d .")
