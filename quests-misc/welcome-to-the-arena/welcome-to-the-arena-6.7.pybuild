# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from pybuild.info import P

from common.fix_maps import FixMaps
from common.mw import MW, File, InstallDir
from common.nexus import NexusMod

# Patch for version 6.6 available here, however no such patch for 6.7 is available
# https://www.nexusmods.com/morrowind/mods/45907


class Package(NexusMod, FixMaps, MW):
    NAME = "Welcome to the Arena"
    DESC = "Lets you fight and bet for fights in Vivec's Arena"
    HOMEPAGE = """
        https://www.nexusmods.com/morrowind/mods/22002
        https://forum.openmw.org/viewtopic.php?f=40&t=6957
    """
    KEYWORDS = "openmw"
    LICENSE = "attribution-derivation"
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/22002"
    RDEPEND = "base/morrowind[bloodmoon,tribunal]"
    TEXTURE_SIZES = "1024"
    SRC_URI = f"""
        Welcome_to_the_Arena-22002-6-7-1589470936.7z
        https://forum.openmw.org/download/file.php?id=1893&sid=47d2fab38be6677470b9031cc2d1fb92
        -> {P}-patch.zip
    """
    NORMAL_MAP_PATTERN = "(_NM|NM)"
    INSTALL_DIRS = [
        InstallDir("Data files", S="Welcome_to_the_Arena-22002-6-7-1589470936"),
        InstallDir(
            ".",
            PLUGINS=[File("Welcome to the Arena! v6.7.esp")],
            S=f"{P}-patch",
        ),
    ]
