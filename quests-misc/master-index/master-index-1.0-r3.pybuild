# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os

from common.mw import MW, File, InstallDir, apply_patch


class Package(MW):
    NAME = "Master Index"
    DESC = "Adds a new quest to find all ten Propylon Indices"
    HOMEPAGE = """
        https://elderscrolls.bethesda.net/en/morrowind
        https://gitlab.com/bmwinger/umopp
    """
    # Original is all-rights-reserved
    # UMOPP is attribution
    LICENSE = "all-rights-reserved attribution"
    RESTRICT = "mirror"
    RDEPEND = "base/morrowind[tribunal]"
    DEPEND = """
        virtual/imagemagick
        >=bin/delta-plugin-0.16
    """
    KEYWORDS = "openmw"
    SRC_URI = """
        https://cdn.bethsoft.com/elderscrolls/morrowind/other/masterindex.zip
        https://gitlab.com/bmwinger/umopp/uploads/473ecbf18aab6fd378244e3c4e6f9889/master_index-umopp-3.2.0.1.tar.xz
    """
    INSTALL_DIRS = [
        InstallDir(".", PLUGINS=[File("master_index.esp")], S="masterindex")
    ]

    def src_prepare(self):
        # From instructions in README.md
        path = os.path.join(self.WORKDIR, "master_index-umopp-3.2.0.1")
        self.execute("delta_plugin apply " + os.path.join(path, "master_index.patch"))
        os.mkdir("Icons/P")
        self.execute(
            "magick convert Icons/Misc_master_index.tga Icons/P/Misc_master_index.dds"
        )
        os.remove("Icons/Misc_master_index.tga")
        apply_patch(os.path.join(path, "master_index_mesh.patch"))
