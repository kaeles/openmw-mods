# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    DESC = "Pluginless replacer for Dagoth Ur and his brothers."
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/45536"
    LICENSE = "all-rights-reserved"
    NAME = "Divine Dagoths"
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/45536"
    RDEPEND = "base/morrowind"
    IUSE = "hostile-gilvoth +shine +halo-glow +halo-sigils map_normal"
    SRC_URI = """
        Divine_Dagoths-45536-2-1-1582499556.7z
        !halo-sigils? (
            Divine_Dagoths_-_No_Halo_Sigils_(Performance_Meshes)-45536-1-0-1609112400.7z
        )
    """
    INSTALL_DIRS = [
        InstallDir(
            "Data Files",
            PLUGINS=[
                File("Unique Dagoth Brandy & Amulet.ESP"),
                File("Bob's Diverse Dagoths.esp", REQUIRED_USE="!hostile-gilvoth"),
                File(
                    "Bob's Diverse Dagoths - Hostile Gilvoth.esp",
                    REQUIRED_USE="hostile-gilvoth",
                ),
                # File("Bob's Diverse Dagoths - DNGDR.esp"),
                # File("Bob's Diverse Dagoths - DNGDR - Hostile Gilvoth.esp"),
            ],
            S="Divine_Dagoths-45536-2-1-1582499556",
        ),
        InstallDir(
            ".",
            S=(
                "Divine_Dagoths_-_No_Halo_Sigils_"
                "(Performance_Meshes)-45536-1-0-1609112400"
            ),
            REQUIRED_USE="!halo-sigils",
        ),
        InstallDir(
            # This isn't actually required if using the latest OpenMW nightly
            "No Shine - Required for OpenMW",
            S="Divine_Dagoths-45536-2-1-1582499556",
            REQUIRED_USE="!shine",
        ),
        InstallDir(
            "No Halo Glow",
            S="Divine_Dagoths-45536-2-1-1582499556",
            REQUIRED_USE="!halo-glow",
        ),
        InstallDir(
            "No Normal Maps",
            S="Divine_Dagoths-45536-2-1-1582499556",
            REQUIRED_USE="!map_normal",
        ),
    ]
