# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from collections import defaultdict

from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Arktwend"
    DESC = "A total conversion mod set on the continent of Myar"
    HOMEPAGE = """
        https://sureai.net/games/myararanath/
        https://wiki.sureai.net/Myar_Aranath:_Relict_of_Kallidar
        https://www.nexusmods.com/morrowind/mods/45611
    """
    KEYWORDS = "openmw"
    LICENSE = "all-rights-reserved"
    TEXTURE_SIZES = ""
    NEXUS_SRC_URI = """
        https://www.nexusmods.com/morrowind/mods/45611?tab=files&file_id=1000010131 -> Arktwend_game_files-45611-1-00.zip
        https://www.nexusmods.com/morrowind/mods/45611?tab=files&file_id=1000011325 -> Arktwend_General_Fixes-45611-0-4-2.zip
    """
    DEPEND = "bin/7z"
    RDEPEND = "base/morrowind[tribunal,bloodmoon,-plugins]"
    INSTALL_DIRS = [
        InstallDir(
            "Arktwend/Data Files",
            PLUGINS=[File("Arktwend_English.esm")],
            S="Arktwend_game_files-45611-1-00",
        ),
        InstallDir(
            "Arktwend_Fixes/Data Files",
            PLUGINS=[File("Arktwend_Fixes.esp")],
            S="Arktwend_General_Fixes-45611-0-4-2",
        ),
    ]

    def src_prepare(self):
        with open("openmw.cfg") as file:
            for line in file.readlines():
                if line.startswith("fallback="):
                    _, _, section_key_value = line.partition("=")
                    section, _, key_value = section_key_value.partition("_")
                    key, _, value = key_value.partition(",")
                    self.FALLBACK = defaultdict(dict)
                    self.FALLBACK[section][key] = value
