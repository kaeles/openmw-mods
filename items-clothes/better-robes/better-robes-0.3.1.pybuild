# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Better Robes"
    DESC = "Replaces robe meshes with more smooth versions"
    HOMEPAGE = """
        https://www.nexusmods.com/morrowind/mods/42773
        tr? ( https://www.nexusmods.com/morrowind/mods/44875 )
    """
    KEYWORDS = "openmw"
    # From README: All meshes and esp-files from the mod can be used anywhere without asking
    # The TR patch explicitly follows the same license
    LICENSE = "free-use"
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        || (
            npcs-bodies/better-bodies
            npcs-bodies/roberts-bodies
        )
        tr? ( landmasses/tamriel-rebuilt )
        animated-mw? ( npcs-misc/animated-morrowind )
    """
    NEXUS_SRC_URI = """
        l10n_en? ( https://nexusmods.com/morrowind/mods/42773?tab=files&file_id=1000001779 -> Better_Robes_ENG-42773-0-3-1.7z )
        l10n_ru? ( https://nexusmods.com/morrowind/mods/42773?tab=files&file_id=1000001784 -> Better_Robes_RUS-42773-0-3-1.7z )
        animated-mw? ( https://nexusmods.com/morrowind/mods/42773?tab=files&file_id=1000002176 -> patch_for_Animated_Morrowind-42773-.7z )
        tr? ( https://nexusmods.com/morrowind/mods/44875?tab=files&file_id=1000015821 -> Better_Robes_Tamriel_Data_Patch-44875-1-1-1569081815.7z )
    """
    IUSE = "tr l10n_en l10n_ru animated-mw"
    REQUIRED_USE = "^^ ( l10n_en l10n_ru )"
    INSTALL_DIRS = [
        InstallDir(
            "Data Files",
            PLUGINS=[File("Better Robes.ESP")],
            S="Better_Robes_ENG-42773-0-3-1",
            REQUIRED_USE="l10n_en",
        ),
        InstallDir(
            "Data Files",
            PLUGINS=[File("Better Robes.ESP")],
            S="Better_Robes_RUS-42773-0-3-1",
            REQUIRED_USE="l10n_ru",
        ),
        # Patches for Unique Fiery Replacer
        # http://nexusmods.com/morrowind/mods/25725/
        # InstallDir(
        #     "patch for UFR",
        #     PLUGINS=[File("UFR_v3dot2.esp")],
        #     S="Better_Robes_ENG-42773-0-3-1",
        #    REQUIRED_USE = "l10n_en",
        # ),
        # InstallDir(
        #     "патч для UFR",
        #     PLUGINS=[File("UFR_v3dot2.esp")],
        #     S="Better_Robes_RUS-42773-0-3-1",
        #     REQUIRED_USE = "l10n_ru",
        # ),
        InstallDir(
            ".",
            BLACKLIST=["Better Robes TR_Patch.esp"],
            PLUGINS=[
                # File("UFR_v3dot2_Patch.esp"),
                File("Better Robes_v0.3.1 Patch.esp"),
            ],
            S="patch_for_Animated_Morrowind-42773-",
            REQUIRED_USE="animated-mw",
        ),
        InstallDir(
            ".",
            PLUGINS=[File("Better Robes TR.esp")],
            S="Better_Robes_Tamriel_Data_Patch-44875-1-1-1569081815",
            REQUIRED_USE="tr",
        ),
    ]
