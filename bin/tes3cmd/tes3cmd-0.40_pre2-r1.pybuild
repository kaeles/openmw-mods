# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import shutil

from pybuild import Pybuild2, patch_dir
from pybuild.info import PN, PV


class Package(Pybuild2):
    NAME = "tes3cmd"
    DESC = "A command line tool to examine and manipulate Morrowind plugins"
    LICENSE = "MIT"
    KEYWORDS = "openmw tes3mp"
    HOMEPAGE = "https://github.com/john-moonsugar/tes3cmd"
    MY_PV = PV.replace("_pre", "-pre-release-")
    SRC_URI = f"""
        !platform_win32? (
            https://github.com/john-moonsugar/{PN}/archive/v{MY_PV}.tar.gz
            -> {PN}-{MY_PV}.tar.gz
        )
        platform_win32? (
            https://github.com/john-moonsugar/{PN}/releases/download/v{MY_PV}/{PN}.exe
        )
    """
    IUSE = "platform_win32"
    S = f"{PN}-{MY_PV}"
    RDEPEND = "!platform_win32? ( sys-bin/perl )"

    def src_unpack(self):
        if "platform_win32" in self.USE:
            os.makedirs(os.path.join(self.WORKDIR, self.S))
            for source in self.A:
                shutil.copy(
                    source.path, os.path.join(self.WORKDIR, self.S, source.name)
                )
        else:
            super().src_unpack()

    def src_install(self):
        os.makedirs(os.path.join(self.D, "bin"))
        if "platform_win32" in self.USE:
            print("Installing tes3cmd.exe into bin")
            os.rename("tes3cmd.exe", os.path.join(self.D, "bin", "tes3cmd.exe"))
        else:
            # Fix weird directory structure
            patch_dir(self.S, ".")
            shutil.rmtree(self.S)
            # Move binary to bin dir
            print("Installing tes3cmd into bin")
            os.rename(self.PN, os.path.join(self.D, "bin", self.PN))
        super().src_install()
