# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from pybuild.info import P

from common.mw import MW, File, InstallDir
from common.util import CleanPlugin


class Package(CleanPlugin, MW):
    NAME = "Ozzy's Grass Mod"
    DESC = "Adds animated grass to various regions of Vvardenfell"
    HOMEPAGE = "https://web.archive.org/web/20161103115849/http://mw.modhistory.com/download-87-14671"
    # From READMEs:
    # © Ozzy 2010 You are free to copy, modify and upload my mod for good.
    LICENSE = "free-use"
    RDEPEND = """
        base/morrowind[bloodmoon?]
        modules/openmw-config[grass]
        !!arch-towns/kilcundas-balmora
        bloodmoon? (
            !!base/tomb-of-the-snow-prince[grass]
            !!land-flora/remiros-groundcover[solstheim]
        )
        !!land-flora/remiros-groundcover[-minimal]
        !!land-flora/vurts-groundcover[-minimal]
        !!land-flora/aesthesia-groundcover
    """
    TEXTURE_SIZES = "362"
    SRC_URI = f"https://web.archive.org/web/20161103115849/http://mw.modhistory.com/file.php?id=14671 -> {P}.7z"
    KEYWORDS = "openmw"
    IUSE = """
        +ascadian-isles
        +west-gash
        +bloodmoon
        +grazelands
        +bitter-coast
    """
    INSTALL_DIRS = [
        InstallDir(
            "Data Files",
            GROUNDCOVER=[
                # Note: Cleaning is really slow for this mod.
                # Probably a fault of tes3cmd, exaggerated by
                # the large size of these plugins
                File(
                    "Ozzy's Grass - Ascadian Isles.esp",
                    CLEAN=True,
                    REQUIRED_USE="ascadian-isles",
                ),
                File(
                    "Ozzy's Grass - West Gash.esp", CLEAN=True, REQUIRED_USE="west-gash"
                ),
                File("Ozzy's Grass - Solstheim.esp", REQUIRED_USE="bloodmoon"),
                File(
                    "Ozzy's Grass - Grazelands.esp",
                    CLEAN=True,
                    REQUIRED_USE="grazelands",
                ),
                File(
                    "Ozzy's Grass - Bitter Coast.esp",
                    CLEAN=True,
                    REQUIRED_USE="bitter-coast",
                ),
            ],
            ARCHIVES=[File("Ozzy's Grass.bsa")],
        )
    ]
