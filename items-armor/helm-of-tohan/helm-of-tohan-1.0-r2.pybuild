# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os

from common.mw import MW, File, InstallDir, apply_patch


class Package(MW):
    NAME = "Helm of Tohan"
    DESC = "Includes a new quest to find the titular, legendary artifact"
    HOMEPAGE = """
        https://elderscrolls.bethesda.net/en/morrowind
        https://gitlab.com/bmwinger/umopp
    """
    # Original is all-rights-reserved
    # UMOPP is attribution
    LICENSE = "all-rights-reserved attribution"
    RESTRICT = "mirror"
    RDEPEND = "base/morrowind"
    DEPEND = ">=bin/delta-plugin-0.15"
    KEYWORDS = "openmw"
    SRC_URI = """
        https://cdn.bethsoft.com/elderscrolls/morrowind/other/ebartifact.zip
        https://gitlab.com/bmwinger/umopp/uploads/a31ea3c395e8c063bb5aa6cc6ae3a28b/helmoftohan-umopp-3.2.0.tar.xz
    """
    INSTALL_DIRS = [InstallDir(".", PLUGINS=[File("EBQ_Artifact.esp")], S="ebartifact")]

    def src_prepare(self):
        # From instructions in README.md
        path = os.path.join(self.WORKDIR, "helmoftohan-umopp-3.2.0")
        self.execute(
            "delta_plugin -v apply " + os.path.join(path, "EBQ_Artifact.patch")
        )
        apply_patch(os.path.join(path, "EB_Helm_of_Tohan_mesh.patch"))
        os.remove("Icons/A/TX_EB_helm_of_Tohan.tga")
        os.remove("Textures/TX_EB_Helm of Tohan.BMP")
