# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from common.mw import MW, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Hlaalu - Arkitektora Vol.2"
    DESC = "High resolution textures of Great House Hlaalu"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/46246"
    KEYWORDS = "openmw"
    LICENSE = "attribution"
    TEXTURE_SIZES = "1024 2048"
    NEXUS_SRC_URI = """
        texture_size_1024? (
            https://www.nexusmods.com/morrowind/mods/46246?tab=files&file_id=1000012911
            -> Hlaalu_Arkitektora_-_MQ-46246-2-0-1545762851.rar
        )
        texture_size_2048? (
            https://www.nexusmods.com/morrowind/mods/46246?tab=files&file_id=1000012907
            -> Hlaalu_Arkitektora_-_HQ-46246-2-0-1545762177.rar
        )
        atlas? (
            texture_size_1024? (
                https://www.nexusmods.com/morrowind/mods/46246?tab=files&file_id=1000012910
                -> Hlaalu_Arkitektora_-_ATLAS_MQ-46246-1-0-1545762798.rar
            )
            texture_size_2048? (
                https://www.nexusmods.com/morrowind/mods/46246?tab=files&file_id=1000012909
                -> Hlaalu_Arkitektora_-_ATLAS_HQ-46246-1-0-1545762751.rar
            )
        )
        map_normal? (
            https://www.nexusmods.com/morrowind/mods/46400?tab=files&file_id=1000013526
            -> Hlaalu_-_Arkitektora_Vol.2_Normal_Maps_-_HQ-46400-1-0-1550634315.rar
            atlas? (
                texture_size_1024? (
                    https://www.nexusmods.com/morrowind/mods/46400?tab=files&file_id=1000013528
                    -> Hlaalu_Arkitektora_-_ATLAS_MQ-46400-1-0-1550635311.rar
                )
                texture_size_2048? (
                    https://www.nexusmods.com/morrowind/mods/46400?tab=files&file_id=1000013527
                    -> Hlaalu_-_Arkitektora_-_ATLAS_Normals_HQ-46400-1-0-1550635035.rar
                )
            )
        )
    """
    RDEPEND = """
        atlas? ( assets-misc/project-atlas )
    """
    IUSE = "atlas map_normal"
    INSTALL_DIRS = [
        InstallDir(
            ".",
            S="Hlaalu_Arkitektora_-_MQ-46246-2-0-1545762851",
            REQUIRED_USE="texture_size_1024",
        ),
        InstallDir(
            ".",
            S="Hlaalu_Arkitektora_-_HQ-46246-2-0-1545762177",
            REQUIRED_USE="texture_size_2048",
        ),
        InstallDir(
            ".",
            S="Hlaalu_Arkitektora_-_ATLAS_MQ-46246-1-0-1545762798",
            REQUIRED_USE="texture_size_1024 atlas",
        ),
        InstallDir(
            ".",
            S="Hlaalu_Arkitektora_-_ATLAS_HQ-46246-1-0-1545762751",
            REQUIRED_USE="texture_size_2048 atlas",
        ),
        InstallDir(
            "Hlaalu - Arkitektora Vol.2 Normal Maps/Data Files",
            S="Hlaalu_-_Arkitektora_Vol.2_Normal_Maps_-_HQ-46400-1-0-1550634315",
            REQUIRED_USE="map_normal",
        ),
        InstallDir(
            "Hlaalu Arkitektora - ATLAS MQ",
            S="Hlaalu_Arkitektora_-_ATLAS_MQ-46400-1-0-1550635311",
            REQUIRED_USE="texture_size_1024 atlas map_normal",
        ),
        InstallDir(
            "Hlaalu Arkitektora - ATLAS Normals HQ",
            S="Hlaalu_-_Arkitektora_-_ATLAS_Normals_HQ-46400-1-0-1550635035",
            REQUIRED_USE="texture_size_2048 atlas map_normal",
        ),
    ]
