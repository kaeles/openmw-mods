# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild.info import PV

from common.mw import DOWNLOAD_DIR, MW, File, InstallDir


class Package(MW):
    NAME = "Tamriel Data"
    DESC = "Adds game data files required by several landmass addition mods"
    HOMEPAGE = """
        http://www.tamriel-rebuilt.org
        https://www.nexusmods.com/morrowind/mods/44537
    """
    LICENSE = "tamriel-data-7.1"
    RDEPEND = "base/morrowind[bloodmoon,tribunal]"
    KEYWORDS = "openmw"
    SRC_URI = f"""
        texture_size_512? ( Tamriel_Data_(vanilla)-{PV}.7z )
        devtools? ( Tamriel_Data_Legacy-44537-06-01.7z )
        texture_size_1024? ( Tamriel_Data_(HD)-{PV}.7z )
    """
    IUSE = "devtools"
    RESTRICT = "fetch"
    TIER = 2

    TEXTURE_SIZES = "512 1024"
    INSTALL_DIRS = [
        # InstallDir(".", S="Tamriel_Data_Animkit_Meshes-44537-1-0a"),
        InstallDir(
            "Data Files",
            ARCHIVES=[File("TR_Data.bsa"), File("PT_Data.bsa")],
            PLUGINS=[File("Tamriel_Data.esm")],
            S=f"Tamriel_Data_(vanilla)-{PV}",
            REQUIRED_USE="texture_size_512",
        ),
        InstallDir(
            "Data Files",
            ARCHIVES=[File("TR_Data.bsa"), File("PT_Data.bsa")],
            PLUGINS=[File("Tamriel_Data.esm")],
            S=f"Tamriel_Data_(HD)-{PV}",
            REQUIRED_USE="texture_size_1024",
        ),
        InstallDir(
            ".",
            PLUGINS=[
                # Note that Tamriel_Data_Legacy should not be enabled in game.
                # This option is included as a development tool.
                # File("Tamriel_Data_Legacy.esm")
            ],
            S="Tamriel_Data_Legacy-44537-06-01",
            REQUIRED_USE="devtools",
        ),
    ]

    def pkg_nofetch(self):
        print("Please download the following files from the urls at the bottom")
        print("before continuing and move them to the download directory:")
        print(f"  {DOWNLOAD_DIR}")
        print()
        for source in self.A:
            print(f"  {source}")
        print()
        if "devtools" in self.USE:
            print("  https://www.nexusmods.com/morrowind/mods/44537/?tab=files")
        if "texture_size_512" in self.USE:
            print(
                "  https://drive.google.com/open?id=1bzzL0iNYRAdAM_-xqUgFrpLl7-yjlBxk"
            )
        elif "texture_size_1024" in self.USE:
            print(
                "  https://drive.google.com/open?id=1Ukb85nnPVeA7M-ubVZlBfAZB8_7hjMuT"
            )
