# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild.info import PN, P

from common.mw import MW, File, InstallDir


class Package(MW):
    NAME = "Tamriel Rebuilt"
    DESC = "A project that aims to fill in the mainland of Morrowind"
    HOMEPAGE = """
        http://www.tamriel-rebuilt.org/
        http://www.nexusmods.com/morrowind/mods/42145
    """
    # No changes (except copyright date) since 20.02
    LICENSE = "tamriel-rebuilt-20.02"
    RESTRICT = "mirror"
    # A list of incompatible mods can be found at
    #   https://www.tamriel-rebuilt.org/incompatible-mods
    # A list of landmass conflicts can be found in the README
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        >=landmasses/tamriel-data-9
        music? ( media-audio/tamriel-rebuilt-music )
        preview? ( !!landmasses/wizards-islands )
        !!land-rocks/on-the-rocks
        firemoth? ( quests-misc/siege-at-fort-firemoth )
        !firemoth? ( !!quests-misc/siege-at-fort-firemoth )
    """
    OLD_P = f"{PN}-22.11"
    KEYWORDS = "openmw"
    SRC_URI = f"""
        https://drive.google.com/uc?id=1a-hRLmkyOwI7jicswMqOypPjLv4463d1
        -> {OLD_P}.7z
        https://drive.google.com/uc?id=1MuvtfrMfZtAsDjcZfedXrYadam4P1YX3
        -> {P}.7z
    """
    IUSE = "preview travels +music firemoth"
    TIER = 2

    INSTALL_DIRS = [
        InstallDir(
            "00 Core",
            PLUGINS=[
                File("TR_Mainland.esm"),
                File("TR_Preview.ESP", REQUIRED_USE="preview"),
                File("TR_Travels.ESP", REQUIRED_USE="!preview travels"),
                File(
                    "TR_Travels_Preview_and_Mainland.ESP",
                    REQUIRED_USE="preview travels",
                ),
                # TODO: For Abot's Travels mods
                #   File("TR_OldTravels.ESP"),
            ],
            S=OLD_P,
        ),
        InstallDir(
            "01 Faction Integration", PLUGINS=[File("TR_Factions.esp")], S=OLD_P
        ),
        InstallDir(
            "02 Siege at Firemoth Compatibility Patch",
            PLUGINS=[File("TR_Firemoth_Vanilla_patch.esp")],
            REQUIRED_USE="firemoth",
            S=OLD_P,
        ),
        InstallDir("00 Core", PLUGINS=[File("TR_Hotfix.esp")], S=P),
    ]
