# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild.info import P

from common.mw import MW, File, InstallDir


class Package(MW):
    NAME = "Tamriel Rebuilt"
    DESC = "A project that aims to fill in the mainland of Morrowind"
    HOMEPAGE = "http://www.tamriel-rebuilt.org/"
    LICENSE = "tamriel-rebuilt-19.12"
    RESTRICT = "mirror"
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        >=landmasses/tamriel-data-7.0
        music? ( media-audio/tamriel-rebuilt-music )
        !!land-rocks/on-the-rocks
    """
    KEYWORDS = "openmw"
    SRC_URI = f"""
        https://drive.google.com/uc?id=1PwfRTmr8ktmJ-lbKr4pc0AeOcn92c2TK&e=download
        -> {P}.7z
    """
    IUSE = "preview travels +music"
    TIER = 2

    INSTALL_DIRS = [
        InstallDir("00 Core", PLUGINS=[File("TR_Mainland.esm")]),
        InstallDir("01 Faction Integration", PLUGINS=[File("TR_Factions.esp")]),
        InstallDir(
            "02 Preview Content",
            PLUGINS=[File("TR_Preview.esp")],
            REQUIRED_USE="preview",
        ),
        InstallDir(
            "03 Travel Network for Core and Vvardenfell",
            PLUGINS=[File("TR_Travels.esp")],
            REQUIRED_USE="travels !preview",
        ),
        InstallDir(
            "03 Travel Network for Core, Preview, and Vvardenfell",
            PLUGINS=[File("TR_Travels_(Preview_and_Mainland).esp")],
            REQUIRED_USE="preview travels",
        ),
        # TODO: Requires Abot's Travels
        # InstallDir(
        #     "04 Patch for Abot's Travels mods",
        #     PLUGINS=[
        #         File("TR_OldTravels.esp", ),
        #     ],
        #     REQUIRED_USE="old-travels"
        # ),
    ]
